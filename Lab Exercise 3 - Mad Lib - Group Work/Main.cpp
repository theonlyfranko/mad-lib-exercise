
// Frank Ontaneda, Nickolas Brown, Luis Pagan
// Mad Lib Exercise

#include <conio.h>
#include <iostream>
#include <string>
#include <fstream>

using namespace std;

void printTeamMadLib(string hippos[10], ostream &out = cout)
{
	std::string madLibLine[10] =
	{
		"My dog is ",
		". He plays ",
		" for the ",
		" Dragons. His rival ",
		" challenged him to a ",
		" competition to win the ",
		". Though the battle was ",
		", my ",
		" named ",
		" saved the day from the evil "
	};

	for (int i = 0; i < 10; i++)
	{
		out << madLibLine[i] << hippos[i];
	}

	out << ".";
}

int main()
{
	cout << "We're going to do a Mad Lib today! \n";

	const int NUM_ENTRIES = 10;
	std::string entry[NUM_ENTRIES] = { "entry1", "entry2", "entry3", "entry4", "entry5", "entry6", "entry7", "entry8", "entry9", "entry10" };
	std::string wordType[NUM_ENTRIES] = 
	{
		"an adjective",
		"a sport",
		"a city name",
		"a name",
		"a competition",
		"a noun",
		"an adjective",
		"an animal",
		"historical figure",
		"a noun (plural)"
	};

	for (int i = 0; i < NUM_ENTRIES; i++)
	{
		cout << "Enter " + wordType[i] + ": ";
		//cin >> entry[i];
		getline(cin, entry[i]);
	}

	cout << "\n";

	/*
	for (int i = 0; i < NUM_ENTRIES; i++)
	{
		std::cout << entry[i] << "\n";
	}
	*/

	printTeamMadLib(entry);

	cout << '\n';
	cout << '\n';

	cout << "Save to file? (y/n): ";
	char input = 'n';
	cin >> input;

	if (input == 'y')
	{
		ofstream ofs("C:\\temp\\mad.txt");
		if (ofs)
		{
			printTeamMadLib(entry, ofs);
		}
		ofs.close();
	}


	_getch();
	return 0;
}